const USUARIO_ENDPOINT = process.env.REACT_APP_USUARIO_ENDPOINT || 'http://localhost:5000/users';


function getUsuarios() {
  return fetch(USUARIO_ENDPOINT);
}

export { getUsuarios };
