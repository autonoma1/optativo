import React, { useState, useEffect } from 'react';
import ListaUsuarios from '../components/ListaUsuarios';
import { getUsuarios } from '../api';


function Usuarios() {
  const [usuarios, setUsuarios] = useState([]);

  useEffect(() => {
    getUsuarios()
      .then(response => {
        return response.json();
      })
      .then(usuarios => {
        setUsuarios(usuarios);
      });
  }, []);

  return (
    <ListaUsuarios usuarios={usuarios} />
  );
}


/*class Usuarios extends Component {
  state = {
    usuarios: [],
  };

  componentDidMount() {
    getUsuarios()
      .then(response => {
        return response.json();
      })
      .then(usuarios => {
        this.setState({ usuarios });
      });
  }

  render() {
    return (
      <ListaUsuarios usuarios={this.state.usuarios} />
    );
  }
};*/

export default Usuarios;
