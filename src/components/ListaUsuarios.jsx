import React from 'react'
import Usuario from './Usuario';

function ListaUsuarios({ usuarios }) {
/*function ListaUsuarios(props) {
  const { usuarios } = props; // const usuarios = props.usuarios;*/

  return (
    <table>
      <thead>
        <tr>
          <th>ID</th>
          <th>E-Mail</th>
          <th>Nombre</th>
        </tr>
      </thead>
      <tbody>
        {usuarios.map((usuario, key) => <Usuario key={key} usuario={usuario} />)}
      </tbody>
    </table>
  );
}

export default ListaUsuarios;
