import React from 'react'

function Usuario({ usuario }) {
  return (
      <tr>
        <td>{usuario.id}</td>
        <td>{usuario.email}</td>
        <td>{usuario.name}</td>
      </tr>
    );
}

export default Usuario;
